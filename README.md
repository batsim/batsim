Batsim
======

The GRICAD Batsim repository has closed.

Batsim should be available on one of the following repos:
- https://github.com/oar-team/batsim
- https://gitlab.inria.fr/batsim/batsim
- https://framagit.org/batsim/batsim
